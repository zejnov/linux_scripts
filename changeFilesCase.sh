#!/bin/bash
#script for changing file names to lower/upper case
echo "Weclome to changeFilesCase.sh_zejnov"

#checking if the argument is for HELP
if [[ $1 == "-h" || $1 == "--help" ]]; then
	echo "Script will change file name as follows:"
	echo "/Script need two arguments 'lower' and 'upper'/"
	echo "Example 1: if you type $ changeFilesCase.sh lower upper"
	echo "	'MyFile' will change to 'MYFILE'"
	echo "Example 2: if you type $ changeFilesCase.sh upper lower"
	echo "	'MyFile' will change to 'myfile'"
	echo "Additionaly, if you type name of file as third parameter"
	echo "script will only change letters in this file"

#our script need 'not null' ('-z') parameters, so let's check it:
elif [ -z "$1" ]; then
	echo "Script need at least two not null parameters"
	echo "Type '-h' or '--help' for more informations"
#our script need proper parameters 'lower' and 'upper':
elif [[ $1 == "lower" && $2 == "upper" || $1 == "upper" && $2 == "lower" ]];
then
	#if third parameter 'is null' we change all files in directory
	if [ -z "$3" ]; then
		#for all files in directory
		for file in $(ls -p|grep -v / ); do
		#'moving file' - only changing file name
		mv $file `echo $file | tr [:$1:] [:$2:]`;
		done;	#end of 'for' loop
	else
		#changing just one file in $3 parameter
		mv $3 `echo $3 | tr [:$1:] [:$2:]`;	
	fi; #end of $3 if/else
else
	echo "Bad parameters given, try '-h' or '--help'"
fi; #end of 'main'  if/elif/else
