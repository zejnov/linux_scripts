#!/bin/bash
#script for changing given phrase in files to other given phrase
echo "Welcome to changePhrase.sh_zejnov"

#checking if the argument is for HELP
if [[ $1 == "-h" || $1 == "--help" ]]; then
	echo "Script will change given phrase in all files in directory"
	echo "to other phrase given as a pharameter"
	echo "Example: $ changePhrase.sh "swearWord" "***""
	echo "Will change all 'swearWord' to '***'"
	

#our script need 'not null' ('-z') parameters, so let's check it:
elif [[ ! -z "$1" && ! -z "$2" ]]; then
	#changing all $1 to $2 recursively

	#just printig the result?! :(
	sed -i'{s|'$1'|'$2'|g}'  $(ls -p|grep -v /)
	
	#found in internet - working
	#find * -type f |xargs perl -pi -e 's|'$1'|'$2'|g'
		
else
	echo "Bad parameters given, try '-h' or '--help'"
fi; #end of 'main' if/elif/else
