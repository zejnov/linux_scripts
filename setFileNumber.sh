#!/bin/bash
#script for enumerating files in folder
echo "Welcome to setFileNumer.sh_zejnov"
counter=0;
#checking if the argument is for HELP
if [[ $1 == "-h" || $1 == "--help" ]]; then
	echo "Script will add enumerator on all files in directory"
	echo "Example: 'myFile' will be changed to '1_myFile'"

else #if no parameter given


#for every file in directory '/' - where script is run
#'ls -p' - choosing directories (append '/' to directory) and then 'grep -v'
#is inverting matching from directories to files
for file in  $(ls -p|grep -v / ); do
	let counter++; #increasing enumerator
	echo "$counter File name: $file" #just printing files founded
	#'moving file' but the directory is not changed, only file names
	mv $file $counter'_'$file 
done;
fi; #end of if/else
